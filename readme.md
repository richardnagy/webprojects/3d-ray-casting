# 3D Ray Casting

A web demo of a traversable 2D map with 3D visuals. 3D effect is achieved with a technique called "ray casting", where for every rendered vertical pixel of the screen, a ray is cast from the camera object and a collision with an object in 2D pace is recorded. The wall is drawn based on the programmed characteristics and distance of the object hit.

## Live Demo

https://richardnagy.gitlab.io/webprojects/3d-ray-casting/